from abc import ABC, abstractmethod, abstractstaticmethod

from bs4 import BeautifulSoup
import undetected_chromedriver as uc
from selenium import webdriver


class AbstractNovel(ABC):
    """
    abstract novel class

    Attributes:
        url: The novel url
        single_thread: A bool represent whether use single thread grab novel information
        volume_name: A string represent the volume name
        volume_number: A string represent the volume number
        book_name: A string represent the book name
        author: A string represent the author
        illustrator: A string represent the illustrator
        introduction: A string represent the introduction
        chapters: A list represent the chapter
        cover_url: A string represent the cover_url
        date: A string represent the date the book last updated (As specified in ISO 8601)
        novel_information: A list contains dict which represent the novel information
    """


    def __init__(self, url, single_thread=False):
        self.url = url
        self.single_thread = single_thread
        self.volume_name = ''
        self.volume_number = ''
        self.author = ''
        self.illustrator = ''
        self.introduction = ''
        self.chapters = []
        self.cover_url = ''
        self.date = ''
        self.novel_information = []

    def __str__(self):
        return '{}:{}'.format(self.__name__, self.url)

    @abstractstaticmethod
    def check_url(url):
        """check whether the url match this website"""
        pass

    def parse_page(self, url, encoding=''):
        """
        parse page with BeautifulSoup

        Args:
            url: A string represent the url to be parsed
            encoding: A string represent the encoding of the html

        Return:
            A BeatifulSoup element
        """
        options = webdriver.ChromeOptions() 
        options.add_argument("--headless")
        options.add_argument('--disable-gpu')
        options.add_argument("start-maximized")
        driver = uc.Chrome(options=options)
        driver.get(url)
        r = driver.page_source
        driver.quit()
        # r.encoding = 'utf-8' if not encoding else encoding
        return BeautifulSoup(r, 'lxml')

    @abstractmethod
    def extract_novel_information(self):
        """extract novel information"""
        pass

    @abstractmethod
    def get_novel_information(self):
        """
        return the novel information

        Return:
            A list contains dict, dict usually has these information: volume_name, volume_number, book_name,
            author, illustrator, introduction, chapters, cover_url, date, source
        """
        pass
